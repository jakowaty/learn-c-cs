#include <stdio.h>
#include <unistd.h>

#include "include/scantools.h"

int main(int argc, char *argv[]) {

	char currentDir[1000];

	if (argc == 1) {
		printf("Please provide pattern to search under current directory");
		return 2;
	}

	if (argc > 2) {
		printf("Please provide exactly one argument - pattern to search under current directory");
		return 3;
	}

	const char *searchPattern = argv[1];

	if (getcwd(currentDir, sizeof(currentDir)) == NULL) {
		printf("Can not obtain current working directory path");
		return 4;
	}

	// A
	// 		1. open directory
	// 		2. scan entries for pattern
	//		3. if entry is directory : A
	//		4. if no entry is directory return

	scan_for_pattern_under_directory(searchPattern, currentDir);

	return 0;
}
