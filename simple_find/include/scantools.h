/*
 * scantools.h
 */

#ifndef INCLUDE_SCANTOOLS_H_
#define INCLUDE_SCANTOOLS_H_

#include <dirent.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int selector(const struct dirent *directory) {
	if (strcmp(directory->d_name, ".") == 0 || strcmp(directory->d_name, "..") == 0) {
		return 0;
	}

	return 1;
}

int match_name(char *string, const char *pattern) {
	regex_t regexp;
	int status;

	if (regcomp(&regexp, pattern, REG_EXTENDED | REG_NOSUB) != 0) {
		return 2;
	}

	status = regexec(&regexp, string, (size_t) 0, NULL, 0);

	regfree(&regexp);

	return (status == 0) ? 0 : 1;
}

/**
 * This gives malloc(): invalid size (unsorted)
 * Probably replace with calloc()
 */
char* create_entry_path(char *cwd, char *entryName) {
	int newSize = strlen(cwd) + strlen(entryName) + 2;
	char *newPath = malloc(newSize);

	strcat(newPath, cwd);
	strcat(newPath, "/");
	strcat(newPath, entryName);

	return newPath;
}

void scan_for_pattern_under_directory(const char *pattern, char *path) {
	struct dirent **entries;
	int filesAmount;

	filesAmount = scandir(path, &entries, selector, alphasort);

	if (filesAmount >= 0) {
		int i, regexError;
		regexError = 0;
		// iterate over all entries:
		for (i = 0; i < filesAmount; ++i) {
			int ret = match_name(entries[i]->d_name, pattern);

			switch (ret) {
				case 0:
					printf("%s: %s --> %s/%s\n", "MATCH", pattern, path, entries[i]->d_name);
					break;
				case 2:
					regexError = 1;
					break;
				default:
					break;
			}

			if (regexError == 1) {
				printf("Regular Expression compilation error!");
				break;
			}

			if (entries[i]->d_type == DT_DIR) {
				size_t l1 = strlen(path);
				size_t l2 = strlen("/");
				size_t l3 = strlen(entries[i]->d_name);
				char *newPath = calloc(l1 + l2 + l3 + 2, 1);

				if (!newPath) {
					printf("Error allocating recurrent path pointer");
					break;
				}

				memcpy(newPath, path, l1);
				strcat(newPath, "/");
				strcat(newPath, entries[i]->d_name);

				scan_for_pattern_under_directory(pattern, newPath);

				free(newPath);
			}

		}
	}
}

#endif /* INCLUDE_SCANTOOLS_H_ */
