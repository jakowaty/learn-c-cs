/*
 * dynamic_array.h
 */
#ifndef INCLUDE_DYNAMIC_ARRAY_H_
#define INCLUDE_DYNAMIC_ARRAY_H_

#include <stdlib.h>

typedef struct {
	char **arr;
	size_t used;
	size_t size;
} dynamic_array_string;

void init_dynamic_array_string(dynamic_array_string *a, size_t initial_size) {
	// init main array with size of initial size of char pointers
	a->arr = (char**) malloc(sizeof(char*) * initial_size);
	a->used = 0;
	a->size = initial_size;
}

void insert_dynamic_array_string(dynamic_array_string *a, char *stringPtr) {
	if (a->size == a->used) {
		a->size *= 2;
		a->arr = realloc(a->arr, (a->size * sizeof(char*)));
	}

	a->arr[a->used++] = stringPtr;
}

void free_dynamic_array_string(dynamic_array_string *a) {
	free(a->arr);
	a->arr = NULL;
	a->size = 0;
	a->used = 0;
}

#endif /* INCLUDE_DYNAMIC_ARRAY_H_ */
